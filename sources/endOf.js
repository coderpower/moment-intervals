var moment = require('moment');
module.exports = function endOf(){
    var date = '2010-06-30';

    var momentObject = moment(date).endOf('week');
    console.log('momentObject: ', momentObject);

    var lastDayOfTheWeek = momentObject.format('l');
    console.log('lastDayOfTheWeek:', lastDayOfTheWeek);

    return lastDayOfTheWeek;
};