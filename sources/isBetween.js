var moment = require('moment');
module.exports = function isBetween(){
    var date = '2010-06-30';
    var start = '2010-12-31';
    var end = '2010-01-25';

    var isBetweenStartAndEnd = moment(date).isBetween(end, start, 'months');
    console.log('isBetweenStartAndEnd: ', isBetweenStartAndEnd);

    return isBetweenStartAndEnd;
};