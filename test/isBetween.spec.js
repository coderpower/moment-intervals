var expect = require('expect.js');
var moment = require('moment');
var isBetween = require('../sources/isBetween.js');

suite('Setting a moment at the end of an interval', function(){

    test('Should return a boolean for "isBetween"', function(){
        var date = '2010-06-30';
        var start = '2010-12-31';
        var end = '2010-01-25';
        var expected = moment(date).isBetween(end, start, 'months');
        var result = isBetween();
        expect(result).to.be(expected);
    });

});