var expect = require('expect.js');
var moment = require('moment');
var endOf = require('../sources/endOf.js');

suite('Setting a moment at the end of an interval', function(){

    test('Should return the last day of the week', function(){
        var date = '2010-06-30';
        var momentObject = moment(date).endOf('week');
        var expected = momentObject.format('l');
        var result = endOf();
        expect(result).to.be(expected);
    });

});