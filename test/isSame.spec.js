var expect = require('expect.js');
var moment = require('moment');
var isSame = require('../sources/isSame.js');

suite('Setting a moment at the end of an interval', function(){

    test('Should return a boolean for "isSame"', function(){
        var date = '2010-06-30';
        var date1 = '2010-06-28';
        var expected = moment(date).isSame(date1, 'week');
        var result = isSame();
        expect(result).to.be(expected);
    });

});