### Intervals in moment.js

Setting a moment at the end of an interval

```javascript
    var momentObject = moment(date).endOf('week');
    var lastDayOfTheWeek = momentObject.format('l');
```
Checking if a moment is within an interval

```javascript
    var date = '2010-06-30';
    var start = '2010-12-31';
    var end = '2010-01-25';
    var isBetweenStartAndEnd = moment(date).isBetween(end, start, 'months');
```
Showcasing shorthand for default intervals

```javascript
    var date = '2010-06-30';
    var date1 = '2010-06-28';
    var isSameWeek = moment(date).isSame(date1, 'week');
```

For more information, please reer to the documentation: http://momentjs.com/docs/#/query/is-same/.